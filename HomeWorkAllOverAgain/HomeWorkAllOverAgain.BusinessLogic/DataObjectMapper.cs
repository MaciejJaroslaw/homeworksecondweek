﻿using HomeWorkAllOverAgain.BusinessLogic.Models;
using HomeWorkAllOverAgain.DataLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkAllOverAgain.BusinessLogic
{
    public interface IDataObjectMapper
    {
        EventBl MapEventToEventBl(Event @event);
        Event MapEventBlToEvent(EventBl eventBl);
        TicketBl MapTicketToTicketBl(Ticket ticket);
        Ticket MapTicketBlToTicket(TicketBl ticketBl);
        PurchaserBl MapPurchaserToPurchaserBl(Purchaser purchaser);
        Purchaser MapPurchaserBlToPurchaser(PurchaserBl purchaserBl);
        IndividualCustomerBl MapIndividualCustomerToIndividualCustomerBl(IndividualCustomer individualCustomer);
        IndividualCustomer MapIndividualCustomerBlToIndividualCustomer(IndividualCustomerBl individualCustomerBl);
        TransactionBl MapTransactionToTransactionBl(Transaction transaction);
        Transaction MapTransactionBlToTransaction(TransactionBl transactionBl);
    }

    public class DataObjectMapper : IDataObjectMapper
    {
        public EventBl MapEventToEventBl(Event @event)
        {
            var eventBl = new EventBl
            {
                organizer = @event.Organizer,
                name = @event.Name,
                description = @event.Description,
                date = @event.Date,
                uid = @event.Id,
                totalAmount = @event.TotalAmount,
                totalLeftAmount = @event.TotalLeftAmount,
                tickets = @event.tickets.Select(MapTicketToTicketBl).ToList(),
                purchasers = @event.purchasers.Select(MapPurchaserToPurchaserBl).ToList()
            };
            return eventBl;
        }

        public Event MapEventBlToEvent(EventBl eventBl)
        {
            var @event = new Event
            {
                Organizer = eventBl.organizer,
                Name = eventBl.name,
                Description = eventBl.description,
                Date = eventBl.date,
                Id = eventBl.uid,
                TotalAmount = eventBl.totalAmount,
                TotalLeftAmount = eventBl.totalLeftAmount,
                tickets = eventBl.tickets.Select(MapTicketBlToTicket).ToList(),
                purchasers = eventBl.purchasers.Select(MapPurchaserBlToPurchaser).ToList()
            };
            return @event;
        }

        public TicketBl MapTicketToTicketBl(Ticket ticket)
        {
            var ticketBl = new TicketBl
            {
                kind = ticket.kind,
                price = ticket.price,
                initialAmount = ticket.initialAmount,
                leftAmount = ticket.leftAmount
            };
            return ticketBl;
        }

        public Ticket MapTicketBlToTicket(TicketBl ticketBl)
        {
            var ticket = new Ticket
            {
                kind = ticketBl.kind,
                price = ticketBl.price,
                initialAmount = ticketBl.initialAmount,
                leftAmount = ticketBl.leftAmount
            };
            return ticket;
        }

        public PurchaserBl MapPurchaserToPurchaserBl(Purchaser purchaser)
        {
            var purchaserBl = new PurchaserBl
            {
                name = purchaser.name,
                surname = purchaser.surname,
                kindOfTicket = purchaser.kindOfTicket,
                age = purchaser.age
            };
            return purchaserBl;
        }

        public Purchaser MapPurchaserBlToPurchaser(PurchaserBl purchaserBl)
        {
            var purchaser = new Purchaser
            {
                name = purchaserBl.name,
                surname = purchaserBl.surname,
                kindOfTicket = purchaserBl.kindOfTicket,
                age = purchaserBl.age
            };
            return purchaser;
        }

        public IndividualCustomerBl MapIndividualCustomerToIndividualCustomerBl(IndividualCustomer individualCustomer)
        {
            var customerBl = new IndividualCustomerBl
            {
                login = individualCustomer.login,
                password = individualCustomer.password,
                name = individualCustomer.name,
                surname = individualCustomer.surname,
                age = individualCustomer.age,
                Id = individualCustomer.Id,
                transactions = individualCustomer.transactions.Select(MapTransactionToTransactionBl).ToList()
            };
            return customerBl;
        }

        public IndividualCustomer MapIndividualCustomerBlToIndividualCustomer(IndividualCustomerBl individualCustomerBl)
        {
            var customer = new IndividualCustomer
            {
                login = individualCustomerBl.login,
                password = individualCustomerBl.password,
                name = individualCustomerBl.name,
                surname = individualCustomerBl.surname,
                age = individualCustomerBl.age,
                Id = individualCustomerBl.Id,
                transactions = individualCustomerBl.transactions.Select(MapTransactionBlToTransaction).ToList()
            };
            return customer;
        }

        public TransactionBl MapTransactionToTransactionBl(Transaction transaction)
        {
            var transactionBl = new TransactionBl
            {
                nameOfEvent = transaction.nameOfEvent,
                kindOfTicket = transaction.kindOfTicket,
                price = transaction.price
            };
            return transactionBl;
        }

        public Transaction MapTransactionBlToTransaction(TransactionBl transactionBl)
        {
            var transaction = new Transaction
            {
                nameOfEvent = transactionBl.nameOfEvent,
                kindOfTicket = transactionBl.kindOfTicket,
                price = transactionBl.price
            };
            return transaction;
        }
    }
}
