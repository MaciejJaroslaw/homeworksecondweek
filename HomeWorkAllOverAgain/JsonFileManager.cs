﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeWorkAllOverAgain.BusinessLogic.Models;
using HomeWorkAllOverAgain.DataLogic.Models;
using Newtonsoft.Json;

namespace HomeWorkAllOverAgain
{
    class JsonFileManager
    {
        public void SaveToFile(string fileName, Event @event, FullStatisticBl stats)
        {
            var json = JsonConvert.SerializeObject(@event, Formatting.Indented);
            var json2 = JsonConvert.SerializeObject(stats, Formatting.Indented);
            File.WriteAllText(fileName, json);
            File.WriteAllText(fileName, json2);
        }
    }
}
