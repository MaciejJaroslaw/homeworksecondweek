﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkAllOverAgain.DataLogic.Models
{
    class Statistic
    {
        public string TicketKind { get; set; }
        public int HighestAge { get; set; }
        public int LowestAge { get; set; }
        public int mediumAge { get; set; }
        public int Benefit { get; set; }
    }
}
