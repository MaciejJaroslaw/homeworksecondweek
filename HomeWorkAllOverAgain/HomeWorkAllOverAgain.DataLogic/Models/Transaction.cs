﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkAllOverAgain.DataLogic.Models
{
    public class Transaction
    {
        public string nameOfEvent { get; set; }
        public string kindOfTicket { get; set; }
        public int price { get; set; }
        public int Id { get; set; }
    }
}
