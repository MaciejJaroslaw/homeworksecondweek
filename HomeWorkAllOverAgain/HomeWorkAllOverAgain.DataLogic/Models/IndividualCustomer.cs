﻿using HomeWorkAllOverAgain.DataLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkAllOverAgain.DataLogic.Models
{
    public class IndividualCustomer
    {
        public string login { get; set; }
        public string password { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string age { get; set; }
        public int Id { get; set; }

        public List<Transaction> transactions { get; set; } = new List<Transaction>();
    }
}
