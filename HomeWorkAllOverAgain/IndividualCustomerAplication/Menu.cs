﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkAllOverAgain
{
    class Menu
    {
        private Dictionary<string, Action> _actionsDictionary = new Dictionary<string, Action>();

        public void AddComand(string name, Action action)
        {
            _actionsDictionary.Add(name, action);
        }

        public void PrintAllComands()
        {
            Console.WriteLine("available commands");

            foreach(String name in _actionsDictionary.Keys)
            {
                Console.WriteLine($"{name}");
            }
        }

        public void RunCommand(string name)
        {
            _actionsDictionary[name]();
        }
    }
}
