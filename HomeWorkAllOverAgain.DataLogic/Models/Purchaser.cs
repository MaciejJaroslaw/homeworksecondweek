﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkAllOverAgain.DataLogic.Models
{
    public class Purchaser
    {
        public string name { get; set; }
        public string surname { get; set; }
        public int eventId { get; set; }
        public string kindOfTicket { get; set; }
        public int age { get; set; }
        public int Id { get; set; }
    }
}
