﻿using HomeWorkAllOverAgain.BusinessLogic.Models;
using HomeWorkAllOverAgain.DataLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using HomeWorkAllOverAgain.DataLogic;

namespace HomeWorkAllOverAgain.BusinessLogic.Services
{
    public interface IEventsService
    {
        void AddEvent(EventBl eventBl, List<TicketBl> ticketsList);
        List<EventBl> GetAllEvents();
        bool checkAvailability(int eventId, string ticketKind);
        void OrderTicket(int eventId, PurchaserBl purchaserBl);
        Event GetEventUid(int eventUid);
        FullStatisticBl GetStatistic(int eventUid);
        void UpdateEvent(Event @event);
    }

    public class EventsService : IEventsService
    {
        private IDataObjectMapper _dataObjectsMapper;
        IEventsService _eventsService;

        public EventsService(IDataObjectMapper dataObjectMapper, IEventsService eventsService)
        {
            _dataObjectsMapper = dataObjectMapper;
        }

        public EventsService()
        {
            _dataObjectsMapper = new DataObjectMapper();
        }

        public void AddEvent(EventBl eventBl, List<TicketBl> ticketsList)
        {
            using (var dbContext = new HomeWorkAllOverAgainDbContext())
            {
                eventBl.tickets = ticketsList;
                dbContext.EventsDbSet.Add(_dataObjectsMapper.MapEventBlToEvent(eventBl));
                dbContext.SaveChanges();
            }
        }
       
        public List<EventBl> GetAllEvents()
        {
            using (var dbContext = new HomeWorkAllOverAgainDbContext())
            {
                var list = dbContext.EventsDbSet;
                var listBl = new List<EventBl>();

                foreach (Event @event in list)
                {
                    listBl.Add(_dataObjectsMapper.MapEventToEventBl(@event));
                }
                return listBl;
            }
        }


        public bool checkAvailability(int eventId, string ticketKind)
        {
            Event @event = _eventsService.GetEventUid(eventId);
            EventBl eventBl = _dataObjectsMapper.MapEventToEventBl(@event);
            return (eventBl.tickets.Find(x => x.kind == ticketKind).leftAmount > 0) ;
        }

        public void OrderTicket(int eventId, PurchaserBl purchaserBl)
        {
            Event @event = _eventsService.GetEventUid(eventId);
            EventBl eventBl = _dataObjectsMapper.MapEventToEventBl(@event);

            eventBl.purchasers.Add(purchaserBl);

            eventBl.tickets.Find(x => x.kind == purchaserBl.kindOfTicket).leftAmount--;
            eventBl.totalLeftAmount--;

            Event eventDl = _dataObjectsMapper.MapEventBlToEvent(eventBl);

            _eventsService.UpdateEvent(eventDl);
        }

        public Event GetEventUid(int eventUid)
        {
            using (var dbContext = new HomeWorkAllOverAgainDbContext())
            {
                return dbContext.EventsDbSet.SingleOrDefault(x => x.Id == eventUid);
            }
        }

        public void UpdateEvent(Event @event)
        {
            using (var dbContext = new HomeWorkAllOverAgainDbContext())
            {
                dbContext.EventsDbSet.Attach(@event);
                dbContext.Entry(@event).State = EntityState.Modified;
                dbContext.SaveChanges();
            }
        }


        public FullStatisticBl GetStatistic(int eventUid)
        {
            Event @event = _eventsService.GetEventUid(eventUid);
            EventBl eventBl = _dataObjectsMapper.MapEventToEventBl(@event);

            FullStatisticBl fullStatisticBl = new FullStatisticBl();
            fullStatisticBl.global.LowestAge = 200;

            int global_total = 0;
            int global_number = 0;

            var ticket_totals = new Dictionary<string, int>();
            var ticket_number = new Dictionary<string, int>();

            foreach (var ticket in eventBl.tickets)
            {
                fullStatisticBl.ticketsStatistic[ticket.kind] = new StatisticBl();
                fullStatisticBl.ticketsStatistic[ticket.kind].LowestAge = 
                    eventBl.purchasers.Count() > 0? 200 : 0;
                ticket_totals[ticket.kind] = 0;
                ticket_number[ticket.kind] = 0;
            }

            foreach(var purchaser in eventBl.purchasers)
            {
                fullStatisticBl.global.HighestAge = Math.Max(purchaser.age, fullStatisticBl.global.HighestAge);
                fullStatisticBl.global.LowestAge = Math.Min(purchaser.age, fullStatisticBl.global.LowestAge);
                global_total += purchaser.age;
                fullStatisticBl.global.Benefit += eventBl.tickets.Find(x => x.kind == purchaser.kindOfTicket).price;

                global_number++;

                fullStatisticBl.ticketsStatistic[purchaser.kindOfTicket].HighestAge = 
                    Math.Max(purchaser.age, fullStatisticBl.ticketsStatistic[purchaser.kindOfTicket].HighestAge);
                fullStatisticBl.ticketsStatistic[purchaser.kindOfTicket].LowestAge =
                    Math.Min(purchaser.age, fullStatisticBl.ticketsStatistic[purchaser.kindOfTicket].LowestAge);
                fullStatisticBl.ticketsStatistic[purchaser.kindOfTicket].Benefit +=
                    eventBl.tickets.Find(x => x.kind == purchaser.kindOfTicket).price;
                ticket_totals[purchaser.kindOfTicket] += purchaser.age;

                ticket_number[purchaser.kindOfTicket]++;
            }

            fullStatisticBl.global.MediumAge =
                global_number > 0 ?
                global_total / global_number :
                0;

            foreach(var stat in fullStatisticBl.ticketsStatistic)
            {
                fullStatisticBl.ticketsStatistic[stat.Key].MediumAge =
                    ticket_number[stat.Key] > 0 ?
                    ticket_totals[stat.Key] / ticket_number[stat.Key] :
                    0;

                if(ticket_number[stat.Key] == 0)
                {
                    fullStatisticBl.ticketsStatistic[stat.Key].LowestAge = 0;
                }
               
            }
            return fullStatisticBl;
        }
    }
}