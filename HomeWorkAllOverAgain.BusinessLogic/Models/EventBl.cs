﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkAllOverAgain.BusinessLogic.Models
{
    public class EventBl
    {
        public string organizer;
        public string name;
        public DateTime date;
        public string description;
        public int totalLeftAmount;
        public int totalAmount;
        public int uid;

        public List<PurchaserBl> purchasers = new List<PurchaserBl>();
        public List<TicketBl> tickets = new List<TicketBl>();
    }
}
