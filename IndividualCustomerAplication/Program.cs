﻿using HomeWorkAllOverAgain.BusinessLogic.Models;
using HomeWorkAllOverAgain.BusinessLogic.Services;
using HomeWorkAllOverAgain.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkAllOverAgain
{
    class Program
    {
        private IndividualCustomersService _individualCustomersService = new IndividualCustomersService();
        private EventsService _eventsService = new EventsService();
        private Menu _menu = new Menu();
        private Menu _internalMenu = new Menu();
        private bool quit;
        private bool logged_in = false;
        private string login;

        static void Main(string[] args)
        {
            new Program().Run();
        }

        public void Run()
        {
            _menu.AddComand("Login", Login);
            _menu.AddComand("Registration", Registration);
            _menu.AddComand("Exit", Exit);

            _internalMenu.AddComand("order new tickets", BuyTickets);
            _internalMenu.AddComand("See history of transaction", SeeTransactions);
            _internalMenu.AddComand("Exit", Exit);

            while (!quit)
            {
                Menu current_menu = logged_in ? _internalMenu : _menu;
                current_menu.PrintAllComands();
                var command = IoHelper.GetStringFromUser("select command");
                current_menu.RunCommand(command);
            }
        }

        public void Login()
        {
            string tryLogin = IoHelper.GetStringFromUser("Enter your login");
            string password = IoHelper.GetStringFromUser("Enter your password");

            if(true)
            {
                logged_in = true;
                login = tryLogin;
                Console.WriteLine("Welcome in the system");
                Console.WriteLine("\n");
            }         
        }

        public void Registration()
        {
            var newUser = new IndividualCustomerBl
            {
                login = IoHelper.GetStringFromUser("Enter your login"),
                password = IoHelper.GetStringFromUser("Enter your password")
            };
            try
            {
                _individualCustomersService.AddCustomer(newUser);
            }
            catch (Exception e)
            {
                Console.WriteLine($">{e.Message}");
            }
        }

        public void Exit()
        {
            quit = true;
        }

        public void BuyTickets()
        {
            Console.WriteLine("List of available events:");
            var events = _eventsService.GetAllEvents();
            int j = 0;
            foreach (EventBl eventBl in events)
            {
                j++;
                Console.WriteLine($"{j} { eventBl.name}");
            }

            Console.WriteLine("select the event from list");
            int selectedNumber = int.Parse(Console.ReadLine());
            var chosenEvent = events[selectedNumber - 1];
            int ticketId;

            for (int i = 0; i < selectedNumber; i++)
            {
                var newPurchaser = new PurchaserBl();
                do
                {
                    ticketId = -1;
                    Console.WriteLine("available kind of ticket");
                    int z = 0;
                    foreach (TicketBl ticketBl in chosenEvent.tickets)
                    {
                        z++;
                        Console.WriteLine($"[{z}] {ticketBl.kind}");
                    }
                    Console.WriteLine("select kind of ticket");
                    ticketId = int.Parse(Console.ReadLine());

                    if (!_eventsService.checkAvailability(chosenEvent.uid, chosenEvent.tickets[ticketId - 1].kind))
                    {
                        ticketId = -1;
                        Console.WriteLine("We have not enough this kind of ticket, you should to selected other kind");
                    }
                }
                while (ticketId == -1);

                newPurchaser.kindOfTicket = chosenEvent.tickets[ticketId - 1].kind;
                newPurchaser.name = IoHelper.GetStringFromUser("Enter the name of purchaser");
                newPurchaser.surname = IoHelper.GetStringFromUser("Enter the surname of purchaser");
                newPurchaser.age = IoHelper.GetIntFromUser("Enter the age of purchaser");

                var transactionBl = new TransactionBl
                {
                    nameOfEvent = chosenEvent.name,
                    kindOfTicket = newPurchaser.kindOfTicket,
                    price = chosenEvent.tickets[ticketId - 1].price
                };

                _eventsService.OrderTicket(chosenEvent.uid, newPurchaser);
                _individualCustomersService.RecordTransaction(login, transactionBl);
            }
        }

        public void SeeTransactions()
        {
            var transactions = _individualCustomersService.GetTransactions(login);

            foreach (TransactionBl transactionBl in transactions)
            {
                Console.WriteLine($"name of event: {transactionBl.nameOfEvent}");
                Console.WriteLine($"kind of ticket: {transactionBl.kindOfTicket}");
                Console.WriteLine($"price of ticket: {transactionBl.price}");
                Console.WriteLine($"\n");
            }
        }
    }
}
